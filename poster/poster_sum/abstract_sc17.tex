\documentclass[sigchi]{acmart}
\usepackage{booktabs} % For formal tables
\usepackage{ccicons}  % For Creative Commons citation icons

% Copyright
%\setcopyright{none}
\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\setcopyright{usgov}
%\setcopyright{usgovmixed}
%\setcopyright{cagov}
%\setcopyright{cagovmixed}


% DOI
\acmDOI{}

% ISBN
\acmISBN{123-4567-24-567/08/06}

%Conference
\acmConference[SC17]{Supercomputing 2017 conference}{November 2017}{Denver, Colorado USA} 
\acmYear{2017}
\copyrightyear{2017}

\acmPrice{}

% Document starts
\begin{document}
% Title portion. Note the short title for running heads 
\title[FleCSPH: Parallel and Distributed SPH Implementation]{FleCSPH: A Parallel and Distributed Smoothed Particle Hydrodynamics Implementation Based on FleCSI Framework}  

\author{Julien Loiseau}
\orcid{1234-5678-9012-3456}
\affiliation{%
  \institution{Laboratory of CReSTIC \\
University of Reims Champagne-Ardenne}
  \country{France}}
\author{Hyun Lim} 
\affiliation{%
 \institution{Department of Physics and Astronomy \\
Brigham Young University}
 \country{USA}}

\author{Ben K. Bergen} 
\affiliation{%
 \institution{Applied Computer Science Group (CCS-7) \\
Los Alamos National Laboratory}
 \country{USA}}

\author{Nicholas D. Moss} 
\affiliation{%
 \institution{Applied Computer Science Group (CCS-7) \\
Los Alamos National Laboratory}
 \country{USA}}
 
 
 
 
 
\begin{abstract}
In this work, we introduce our new parallel and distributed Smoothed Particle Hydrodynamics implementation, FleCSPH, which is based on the open-source framework FleCSI. This framework provides us the basic data structures and runtime required in our work. We intend to provide a parallel and distributed version of Binary, Quad and Oct trees data structure, respectively for 1, 2 and 3 dimensions dedicated for SPH problems. Also, we present various test problems in several dimensions that indicate the flexibility and the scalability of our toolkit.
For application and tests we simulate classical physics test cases like Sod Shock Tube, Sedov Blast Wave or Fluid Flows.
The aim of this work is to simulate binary compact object mergers such as white dwarfs and neutron stars that address many interesting astrophysical phenomena in the universe. 
Due to the complexity of this multi-physics problem, very high accurate and stable simulations are required. 
Our code will be suited for solving a variety of compact binary merger scenarios with the extraction of gravitational wave signal, accretion rates, ejected matter and more 
\end{abstract}


\keywords{FleCSI, SPH, Astrophysics}

\maketitle

\section{Smoothed Particle Hydrodynamics}
Smoothed Particle Hydrodynamics (SPH) is explicit numerical mesh-free method that solves hydrodynamical partial differential equations by discretizing in set of fluid elements called particle. \cite{rosswog2009,springel2005cosmological,monaghan1992smoothed,liu2010smoothed,gingold1977smoothed,warren20132hot}
The fundamental SPH formulation is:
\begin{equation}
\langle A \rangle (\vec{r}) \simeq \sum_b \frac{m_b}{\rho_b} A(\vec{r}_b) W( | \vec{r} - \vec{r}_b|,h)
\end{equation}
where $W$ is the smoothing kernel, $h$ is the smoothing length (hydro interaction range) that evolved for each particle.

In this work, we want to solve the Lagrangian conservation equations for mass, energy, and momentum of an ideal fluid.
By using the volume element $V_b = m_b/ \rho_b$, we can formulate SPH scheme such that:

\begin{equation}
  	\rho_a = \sum_b m_b W_{ab}(h_a) 
\end{equation}

\begin{equation}
  	\frac{d u_a}{dt} = \frac{P_a}{\rho_a^2} \sum_b m_b \vec{v}_{ab} \cdot \nabla_a W_{ab} 
\end{equation}

\begin{equation}
  	\frac{\vec{v}_a}{dt} = - \sum_b m_b \left( \frac{P_a}{\rho_a^2} + \frac{P_b}{\rho_b^2} \right) \nabla_a W_{ab}
\end{equation}

\section{FleCSPH}

FleCSPH\footnote{http://github.com/laristra/flecsph} is based on the FleCSI\footnote{http://github.com/laristra/flecsi} runtime.
FleCSI is a compile-time configurable framework designed to support multi-physics application development. As such, FleCSI provides a very general set of infrastructure design patterns that can be specialized and extended to suit the needs of a broad variety of solver and data requirements. FleCSI currently supports multi-dimensional mesh topology, geometry, and adjacency information, as well as n-dimensional hashed-tree data structures, graph partitioning interfaces, and dependency closures.

\begin{figure}[t]
  \includegraphics[width=\linewidth]{figures/algo.png}
  \caption{Distributed quad-tree}\label{fig:tree}
  \end{figure}
\begin{figure}[t]
  \includegraphics[width=.9\linewidth]{figures/fmm.png}
  \caption{Multipole method}\label{fig:fmm}

\end{figure}

FleCSPH handles several aspects of the SPH simulations. 
First, the IO parallel format is based on H5Hut \cite{howison2010h5hut} and H5Part. 
Which handles to read and write large dataset of particles. 

Then, base on FleCSI parallel tree topology, the distribution is done using MPI searching for EXCLUSIVE, SHARED or GHOSTS particles for every processes. 
Respectively represented on red, blue and green on the quad-tree extracted on Fig.\ref{fig:tree}. 
The tree implementation is based on Barnes and Hut tree topology\cite{barnes1986hierarchical,barnes1990modified}. 

Several \textit{Kernel} and their gradients are available for SPH computation. We also provide physics implementation and intent to implement in an optimal way several kind of EOS from analytics to tabulated. 

Also, as a requirement for astrophysics simulations, we incorporated a gravitation interaction computation based on the fast multipole method algorithm plotted on Fig.\ref{fig:fmm}.
The tree is decomposed for every processes from multipole to multipole and then the extension for multipole to local particles. 
The tree search is based on a Multipole Acceptance Criterion $\theta$ and the COM, Center Of Mass, data for each branch of the tree structure.  

\begin{figure}[t]
\includegraphics[width=\linewidth]{figures/results_dgx.png}
\caption{Scalability up to 250000 particles}
\label{fig:results_dgx}
\end{figure}

\section{Test Cases and results}
Here, we aim to solve different types of problems using FleCSPH. 
We test different types of hydrodynamical and astrophysical problems within various dimensions. 
For 1D test case, we present the Sod Shock Tube problem, for 2D, the Sedov Blast Wave and for 3D, a fluid flow. They are all compared with the analytics solution to ensure the correctness of our results. 



Fig.\ref{fig:results_dgx} is the first set of tests for scalability of our code up to 16 processes on DGX platform using E5-2698 v4 2.2GHz CPUs. 
One of the may afterwork in Computer Science side of this project is to be able to efficently use Accelerators like GPUs, Xeon Phi or even FPGA in a distributed way. 

As a final goal, we aim to simulate double white dwarf (DWD) and binary neutron star (BNS) with Newtonian gravity to model different astrophysical phenomena such as gravitational wave template and supernovae.\\

\section{Conclusion}
In this poster session we intent to present the first version of FleCSPH and the test cases results. 
The Domain Decomposition scheme and the tree data structure as well as the distribution and research scheme will be discussed. 
The SPH method and physics test applications results will be described too. 
\newpage
% Bibliography
\bibliographystyle{ACM-Reference-Format}
\bibliography{biblio/sc17_poster}

\end{document}
