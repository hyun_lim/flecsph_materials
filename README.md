# README #

Aug.16.2017 - initiated

Mar.22.2018 - updated

This is a repo for every detail data, notes, etc for FleCSPH project

- The note has LA-UR 17-25908
- The poster has LA-UR 17-26250 for student symposium draft version
- The poster, FleCSPH_poster_Sympo has LA-UR 17-26744 for final version of LANL student symposium
- The poster summay file has LA-UR 17-26249 for SC17 submission
- The poster, FleCSPH_poster_SC17 has LA-UR 17-29074
- The abstract has LA-UR 17-25892 for 2017 LANL student annual symposium
- The paper has LA-UR 18-21167 for HPCS

### Who do I talk to? ###

* For more details, please contact Hyun Lim (hylim1988@gmail.com) or Julien Loiseau (julien.loiseau@univ-reims.fr)
